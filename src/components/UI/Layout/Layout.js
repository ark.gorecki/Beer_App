import React, { Component } from 'react';

import Aux from '../../../hoc/Auxilary/Auxilary';
import Ribbon from '../Ribbon/Ribbon';

import './Layout.css'

class Layout extends Component {
  render() {
    return (
      <Aux>
        <Ribbon />
        <main className="Content">
          <h1 className="Title">BEER<span>GALLERY</span></h1>
          {this.props.children}
        </main>
      </Aux>
    )
  }
};

export default Layout;
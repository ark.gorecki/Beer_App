import React from 'react';

import './Ribbon.css';

const ribbon = props => (
    <div className="Ribbon-Content">
        <div className="Ribbon">
            <span className="Ribbon-text">
                WE
                <span>
                    {' '}
                    <i class="fas fa-beer" />{' '}
                </span>
                BEER
            </span>
        </div>
    </div>
);

export default ribbon;

import React, { Component } from 'react';
import axios from 'axios';

import Aux from '../../../hoc/Auxilary/Auxilary';
import Backdrop from '../../UI/Backdrop/Backdrop';
import Spinner from '../../UI/Spinner/Spinner';
import Thumbnail from '../BeerGallery/Thumbnail/Thumbnail';

import './Modal.css';

class Modal extends Component {
  state = {
    loadedBeer: null,
    loading: false,
    loadingRecommended: false,
    error: false,
    beers: [],
    similarBeers: [],
    id: this.props.match.params.id,
  };

  componentDidMount() {
    this.getBeer();
  }

  getBeer = () => {
    this.setState({
      loading: true,
    });
    const id = this.state.id;
    axios
      .get(`https://api.punkapi.com/v2/beers/${id}`)
      .then(result => {
        this.setState({
          loadedBeer: result.data,
          loading: false,
          shouldUpdate: false,
        });
        this.getSimilarBeers();
      })
      .catch(error => {
        this.setState({
          error: error,
          loading: false,
          shouldUpdate: false,
        });
      });
  };

  getSimilarBeers = () => {
    this.setState({
      loadingRecommended: true,
      shouldUpdate: true,
    });
    const abv = Number(this.state.loadedBeer[0].abv);
    const ebc = Number(this.state.loadedBeer[0].ebc);

    const abvs = [Math.floor(abv - 1), Math.floor(abv + 2)];
    const ebcs = [ebc - 5, ebc + 5];

    axios
      .get(
        `https://api.punkapi.com/v2/beers?ebc_gt=${ebcs[0]}&ebc_lt=${ebcs[1]}&abv_gt=${
          abvs[0]
        }&abv_lt=${abvs[1]}`
      )
      .then(result => {
        const data = result.data.filter(i => {
          return i.name !== this.state.loadedBeer[0].name;
        });
        this.setState({
          similarBeers: data.slice(0, 3),
          loadingRecommended: false,
          shouldUpdate: false,
        });
      })
      .catch(error => {
        console.log(error);
        this.setState({
          error: error,
          loadingRecommended: false,
          shouldUpdate: false,
        });
      });
  };

  clickBack = () => {
    this.props.history.push('/');
  };

  handleChangeBeer = (event, i) => {
    event.preventDefault();
    this.setState(
      {
        shouldUpdate: true,
        id: i,
      },
      () => {
        this.props.history.push(`/beer/${i}`);
        this.getBeer();
      }
    );
    console.log(this.props.history);
    console.log(i);
  };

  render() {
    let modal = <Spinner style={{ Width: '100%', textAlign: 'center', margin: 'auto' }} />;

    let leng = this.state.similarBeers.length > 2 ? true : false;

    let recommended = <Spinner style={{ width: '100%' }} />;
    if (!this.state.loadingRecommended) {
      if (this.state.similarBeers.length > 0) {
        recommended = this.state.similarBeers.map((beer, i) => {
          return (
            <div
              className="Thumbnail Rec-Thumbnail"
              style={{ width: leng ? '30%' : '45%' }}
              onClick={(event, i) => this.handleChangeBeer(event, beer.id)}
              key={i}
            >
              <Thumbnail beer={beer} />
            </div>
          );
        });
      } else {
        recommended = null;
      }
    }

    if (this.state.error) {
      modal = <h1 style={{ margin: '15px' }}>Something went wrong, please try again later</h1>;
    }

    if (this.state.loadedBeer) {
      modal = (
        <Aux>
          <div className="Image">
            <img src={this.state.loadedBeer[0].image_url} alt={this.state.loadedBeer[0].name} />
          </div>
          <div className="Side">
            <h1 className="Beer-Title">{this.state.loadedBeer[0].name}</h1>
            <p className="Beer-Tagline">{this.state.loadedBeer[0].tagline}</p>
            <p className="Beer-Desc">{this.state.loadedBeer[0].description}</p>
            <div className="Break" />
            <div className="Details">
              <p>
                IBU: <span>{this.state.loadedBeer[0].ibu}</span>
              </p>
              <p>
                ABV:<span>{this.state.loadedBeer[0].abv}%</span>
              </p>
              <p>
                EBC: <span>{this.state.loadedBeer[0].ebc}</span>
              </p>
            </div>
            <div className="Tips">
              <p>Brewer Tips:</p>
              <small>"{this.state.loadedBeer[0].brewers_tips}"</small>
            </div>
            <div className="Recommended">
              {recommended ? <p>You might also like:</p> : <p>Sorry, no similar beers to show</p>}
              <div className="Recommended-Container">{recommended}</div>
            </div>
          </div>
        </Aux>
      );
    }

    return (
      <Aux>
        <Backdrop clickBack={this.clickBack} />
        <div className="Modal">{modal}</div>
      </Aux>
    );
  }
}

export default Modal;

import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
import axios from 'axios';
import InfiniteScroll from 'react-infinite-scroller';

import './BeerGallery.css';
import Aux from '../../../hoc/Auxilary/Auxilary';
import Modal from '../Modal/Modal';
import Spinner from '../../UI/Spinner/Spinner';
import Thumbnail from './Thumbnail/Thumbnail';

class BeerGallery extends Component {
  state = {
    beers: [],
    loading: false,
    hasMore: true,
    page: 0,
    error: false
  };

  fetchData = () => {
    let page = this.state.page + 1;
    let url = `https://api.punkapi.com/v2/beers?page=${page.toString()}&per_page=20`;
    axios
      .get(url)
      .then(result => {
        if (result.data.length > 0) {
          this.setState({
            beers: [...this.state.beers, ...result.data],
            page: page
          });
        } else {
          this.setState({
            hasMore: false
          });
        }
      })
      .catch(error => {
        this.setState({ error });
      });
  };

  render() {
    let beers = null;
    let endScrolling = null;
    let loader = <Spinner />;

    if (!this.state.loading && this.state.beers.length > 1) {
      beers = this.state.beers.map((beer, i) => {
        return (
          <div className="Thumbnail" key={i}>
            <Link to={'/beer/' + beer.id}>
              <Thumbnail beer={beer} />
            </Link>
          </div>
        );
      });
    }

    if (this.state.error) {
      beers = <h1>Something went wrong, please try again later</h1>;
    }

    if (!this.state.hasMore) {
      endScrolling = <div className="OutOfData">No more beers to show</div>;
    }
    return (
      <Aux>
        <Route path="/beer/:id" component={Modal} />
        <InfiniteScroll
          pageStart={0}
          loadMore={this.fetchData}
          hasMore={this.state.hasMore}
          loader={loader}
        >
          <div className="Container">{beers}</div>
        </InfiniteScroll>
        {endScrolling}
      </Aux>
    );
  }
}

export default BeerGallery;

import React from 'react';

import './Thumbnail.css'

const Thumbnail = (props) => (
	<div className="Beer_desc">
		<img src={props.beer.image_url} alt={props.beer.name} />
		<p className="Name">{props.beer.name}</p>
		<small className="Tagline">{props.beer.tagline}</small>
	</div>
)

export default Thumbnail;
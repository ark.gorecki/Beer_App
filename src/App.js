import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import Layout from './components/UI/Layout/Layout';
import BeerGallery from './components/Beer/BeerGallery/BeerGallery';

import './App.css';

class App extends Component {
  render() {
    return (
      <div>
        <Layout>
          <Route path="/" component={BeerGallery} />
        </Layout>
      </div>
    );
  }
}

export default App;
